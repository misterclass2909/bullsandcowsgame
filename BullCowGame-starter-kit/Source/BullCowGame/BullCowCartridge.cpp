// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "HiddenWordList.h"
// #include "Math/UnrealMathUtility.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
    Isograms = GetValidWords(HiddenWords);  
    SetupGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	if(bGameOver)
	{
        ClearScreen();
        SetupGame();
        
	}
    else
    {
        ProcessGuess(Input);
    }
} 

void UBullCowCartridge::SetupGame()
{
    //Greeting for user
    PrintLine(TEXT("Hello, My Friend!"));
    PrintLine(TEXT("MY name is MisterClass!"));

    //Setup HiddenWord
    HiddenWord = Isograms[FMath::RandRange(0, Isograms.Num() - 1)];
    PrintLine(HiddenWord);

    //Setup Lives
    LivesCount = HiddenWord.Len();

	//Init game start
    bGameOver = false;

    PrintLine(FString::Printf(TEXT("You have %d lives before"), LivesCount));

    //Ask for user guess (AskGuess) 
    PrintLine(FString::Printf(TEXT("Guess the %d letter word"), HiddenWord.Len()));
}

void UBullCowCartridge::EndGame()
{
    bGameOver = true;
    PrintLine(TEXT("Please type enter to play again!"));
}

void UBullCowCartridge::ProcessGuess(const FString& GuessInput)
{
    int32 InputLength = GuessInput.Len();
    int32 HiddenWordLen = HiddenWord.Len();

    if (GuessInput== HiddenWord)
    {
        PrintLine(TEXT("You win!"));
        EndGame();
        return;
    }

    if (InputLength != HiddenWordLen)
    {
        PrintLine(FString::Printf(TEXT("Hidden word is %d characters long! Try again!"), HiddenWord.Len()));
        return;
    }

    if (!IsIsogram(GuessInput))
    {
        PrintLine(TEXT("NO repeating letters, try again!"));
        return;
    }
    
    if (--LivesCount <= 0)
    {
        PrintLine(TEXT("You lose!"));
        EndGame();
        return;
    }

    FBullCowCount BullCowCount;
    GetBullCows(GuessInput, BullCowCount);

    PrintLine(TEXT("You have %d bulls and %d cows!"), BullCowCount.Bulls, BullCowCount.Cows);

    PrintLine(FString::Printf(TEXT("You have lost one live! Now you have %d lives! Be careful!"), LivesCount));
}

bool UBullCowCartridge::IsIsogram(const FString& Text) const 
{
    for (int i = 0; i < Text.Len() - 1; i++)
    {
        for (int j = i + 1; j < Text.Len(); j++)
        {
            if (Text[i] == Text[j]) return false;
        }
    }

    return true;
}

TArray<FString> UBullCowCartridge::GetValidWords(const TArray<FString>& WordList) const
{
    TArray<FString> ValidWords;

    for (FString Word : WordList)
    {
        if (Word.Len() >= 4 && Word.Len() <= 8 && IsIsogram(Word))
        {
            ValidWords.Emplace(Word);
        }
    }

    return ValidWords;
}

void UBullCowCartridge::GetBullCows(const FString& Guess, FBullCowCount& BullCowCount) const 
{
    BullCowCount.Cows = 0;
    BullCowCount.Bulls = 0;

    for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
    {
        if (Guess[GuessIndex] == HiddenWord[GuessIndex])
        {
            BullCowCount.Bulls++;
            continue;
        }

        for (int32 HiddenIndex = 0; HiddenIndex < HiddenWord.Len(); HiddenIndex++)
        {
            if(Guess[GuessIndex] == HiddenWord[HiddenIndex])
            {
                BullCowCount.Cows++;
                break;
            }
        }
        
    }
    
}